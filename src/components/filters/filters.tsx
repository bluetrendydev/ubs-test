import React from "react";

import { Box, Slider, Typography } from "@mui/material";

import { IServerData } from "./../../types/types";

interface IFilters {
  setMaxSpendings: (value: number) => void;
  data: IServerData[];
}

const filters = (props: IFilters) => {
  const minValue = Math.min(...props.data.map((item) => item.spend));
  const maxValue = Math.max(...props.data.map((item) => item.spend));

  const onChange = (event: Event, value: any) => {
    props.setMaxSpendings(value);
  };

  return (
    <Box my={4}>
      <Typography>Spendings</Typography>
      <Slider
        onChange={onChange}
        defaultValue={12}
        min={minValue}
        max={maxValue}
        aria-label="Default"
        valueLabelDisplay="auto"
      />
      <Box display="flex" justifyContent="space-between">
        <Typography>${minValue}</Typography>
        <Typography>${maxValue}</Typography>
      </Box>
    </Box>
  );
};

export default filters;
