import { Box, Grid, Card } from "@mui/material";

import { IContent, IServerData } from "./../../types/types";

const filterData = (data: IServerData[], max: number) => {
  return data.filter((item) => item.spend >= max);
};

const Content = (props: IContent) => {
  const filteredData: IServerData[] = filterData(
    props.data,
    props.maxSpendings
  );

  return (
    <Box px={10}>
      <Grid container spacing={2}>
        {filteredData.map((item, i) => {
          return (
            <Grid item xs={3} key={i}>
              <Card>
                <Box px={4} py={8} textAlign="center">
                  <div>{item.name}</div>
                  <div>Total Spend: ${item.spend}</div>
                </Box>
              </Card>
            </Grid>
          );
        })}
      </Grid>
    </Box>
  );
};

export default Content;
