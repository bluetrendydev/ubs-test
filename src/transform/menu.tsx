import { FilterType, IData, IServerData } from "./../types/types";

const findIndex = (data: IData[], name: string) => {
  return data.findIndex((item) => item.name === name);
};

const generateMenu = (data: IServerData[]) => {
  let menuData: IData[] = [];
  for (let i in data) {
    const index = findIndex(menuData, data[i].BCAP1);

    if (index === -1) {
      menuData.push({
        key: FilterType.BCAP1,
        name: data[i].BCAP1,
        children: [
          {
            key: FilterType.BCAP2,
            name: data[i].BCAP2,
            children: [
              {
                key: FilterType.BCAP3,
                name: data[i].BCAP3,
                children: [],
              },
            ],
          },
        ],
      });
    } else {
      const index2 = findIndex(menuData[index].children, data[i].BCAP2);

      if (index2 === -1) {
        menuData[index].children.push({
          key: FilterType.BCAP2,
          name: data[i].BCAP2,
          children: [
            {
              key: FilterType.BCAP3,
              name: data[i].BCAP3,
              children: [],
            },
          ],
        });
      } else {
        const index3 = findIndex(
          menuData[index].children[index2].children,
          data[i].BCAP3
        );

        if (index3 === -1) {
          menuData[index].children[index2].children.push({
            key: FilterType.BCAP3,
            name: data[i].BCAP3,
            children: [],
          });
        }
      }
    }
  }

  //sort data
  menuData.sort((a, b) => a.name.localeCompare(b.name));

  for (let j in menuData) {
    menuData[j].children.sort((a, b) => a.name.localeCompare(b.name));

    for (let k in menuData[j].children) {
      menuData[j].children[k].children.sort((a, b) =>
        a.name.localeCompare(b.name)
      );
    }
  }

  return menuData;
};

export { generateMenu };
