import React from "react";

import HomePage from "./pages/homepage/homepage";

const App = () => {
  return (
    <div>
      <h1>Pharos Coding Exercise</h1>
      <HomePage />
    </div>
  );
};

export default App;
