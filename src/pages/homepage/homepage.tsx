import React, { useEffect, useState } from "react";

import axios from "axios";

import {  Grid, Divider } from "@mui/material";

import Menu from "./../../common/menu/menu";

import Content from "./../../components/content/content";
import Filters from "./../../components/filters/filters";

import { IServerData, IFilter, FilterType } from "./../../types/types";

const serverURL = "/data";

const filterData = (
  data: IServerData[],
  key: FilterType | undefined,
  value: string | undefined
) => {
  if (key && value) {
    let filtered = data.filter((item) => item[key] === value);
    return filtered;
  }

  return data;
};

const HomePage = () => {
  const [data, setData] = useState<IServerData[]>();
  const [maxSpendigs, setMaxSpendings] = useState(-1);
  const [filteredData, setFilteredData] = useState<IServerData[]>();
  const [filter, setFilter] = useState<IFilter>({
    key: undefined,
    value: undefined,
  });

  const selectFilter = (key: FilterType, value: string) => {
    setFilter({ key, value });
  };

  useEffect(() => {
    if (data) {
      const filtered = filterData(data, filter.key, filter.value);
      setFilteredData(filtered);
    }
  }, [data, filter]);

  useEffect(() => {
    const getData = () => {
      axios
        .get(serverURL)
        .then((response) => {
          setData(response.data);
        })
        .catch(() => {
          console.log("error loading data");
        });
    };
    getData();
  }, []);

  return (
    <React.Fragment>
      {data && filteredData && (
        <Grid container>
          <Grid item xs={2}>
            <Menu data={data} filter={filter} selectFilter={selectFilter} />
            <Divider />
            <Filters data={filteredData} setMaxSpendings={setMaxSpendings} />
          </Grid>

          <Grid item xs={10}>
            <Content data={filteredData} maxSpendings={maxSpendigs} />
          </Grid>
        </Grid>
      )}
    </React.Fragment>
  );
};

export default HomePage;
