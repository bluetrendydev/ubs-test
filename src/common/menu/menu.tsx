import React from "react";

import { List } from "@mui/material";

import MenuList from "./menu-list";

import data from "../../data.json";

import { generateMenu } from "../../transform/menu";

import { IFilter, FilterType, IServerData } from "./../../types/types";

interface IMenu {
  data: IServerData[];
  filter: IFilter;
  selectFilter: (key: FilterType, value: string) => void;
}

const Menu = (props: IMenu) => {
  const menu = generateMenu(data);

  return (
    <React.Fragment>
      <List sx={{ width: "100%", maxWidth: 300 }}>
        {menu.map((item, i) => {
          return (
            <MenuList
              id={0}
              key={i}
              keySelected={item.key}
              name={item.name}
              children={item.children}
              filter={props.filter}
              selectFilter={props.selectFilter}
            />
          );
        })}
      </List>
    </React.Fragment>
  );
};

export default Menu;
