import React, { useState } from "react";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import { Collapse, ListItemButton, ListItemText } from "@mui/material";

import { IFilter, FilterType } from "./../../types/types";

interface IMenu {
  id: number;
  keySelected: FilterType;
  name: string;
  children: any[];
  filter: IFilter;
  selectFilter: (key: FilterType, value: string) => void;
}

const MenuList = (props: IMenu) => {
  const [open, setOpen] = useState(false);

  const onClick = (key: FilterType, value: string) => {
    props.selectFilter(key, value);
    setOpen(!open);
  };

  const paddingLeft = props.id * (props.children ? 2 : 4);
  return (
    <React.Fragment>
      <ListItemButton
        selected={props.filter.value === props.name}
        onClick={() => onClick(props.keySelected, props.name)}
        sx={{ pl: paddingLeft }}
      >
        {props.children.length !== 0 && (
          <React.Fragment>
            {open ? <ExpandLess /> : <ExpandMore />}
          </React.Fragment>
        )}
        <ListItemText primary={props.name} />
      </ListItemButton>

      {props.children &&
        props.children.map((item, i) => {
          return (
            <Collapse key={i} in={open} unmountOnExit>
              <MenuList
                id={props.id + 1}
                keySelected={item.key}
                name={item.name}
                children={item.children}
                filter={props.filter}
                selectFilter={props.selectFilter}
              />
            </Collapse>
          );
        })}
    </React.Fragment>
  );
};

export default MenuList;
