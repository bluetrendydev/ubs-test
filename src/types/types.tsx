export enum FilterType {
  BCAP1 = "BCAP1",
  BCAP2 = "BCAP2",
  BCAP3 = "BCAP3",
}

export interface IFilter {
  key?: FilterType;
  value?: string;
}

export interface IContent {
  data: IServerData[];
  maxSpendings: number;
}

export interface IServerData {
  id: string;
  name: string;
  spend: number;
  BCAP1: string;
  BCAP2: string;
  BCAP3: string;
}

export interface IData {
  key: FilterType;
  name: string;
  children: IData[];
}
